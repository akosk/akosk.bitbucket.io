(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mom-board></mom-board>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'mom';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'mom-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_board_board_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/board/board.component */ "./src/app/components/board/board.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @progress/kendo-angular-inputs */ "./node_modules/@progress/kendo-angular-inputs/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @progress/kendo-angular-buttons */ "./node_modules/@progress/kendo-angular-buttons/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @progress/kendo-angular-layout */ "./node_modules/@progress/kendo-angular-layout/dist/es/index.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _components_board_board_component__WEBPACK_IMPORTED_MODULE_4__["BoardComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_6__["InputsModule"],
                _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_6__["SwitchModule"],
                _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_9__["ButtonsModule"],
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_8__["DropDownsModule"],
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_7__["GridModule"],
                _progress_kendo_angular_layout__WEBPACK_IMPORTED_MODULE_10__["PanelBarModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/board/board.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/board/board.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"display:flex\">\n  <div id=\"chart\"></div>\n  <div id=\"controller\">\n    <div style=\"text-align: center\"><img src=\"/assets/dd.jpg\" style=\"width:180px\"></div>\n\n\n    <kendo-panelbar>\n      <kendo-panelbar-item [title]=\"'Current turn'\" expanded=\"true\">\n        <ng-template kendoPanelBarContent>\n          <div class=\"panel-content\" style=\"display: flex; flex-direction: column;justify-content:center; align-content:center; align-items:center\">\n            <span>{{game.getTurnTeam() | uppercase}}'s turn</span>\n            <button kendoButton (click)=\"finishTurn()\">Finish turn</button>\n          </div>\n        </ng-template>\n      </kendo-panelbar-item>\n      <kendo-panelbar-item [title]=\"'Selected cell'\" expanded=\"true\">\n        <ng-template kendoPanelBarContent>\n          <table>\n            <tr>\n              <th>Row</th>\n              <td>{{selectedCell?.row}}</td>\n            </tr>\n            <tr>\n              <th>Column</th>\n              <td>{{selectedCell?.col}}</td>\n            </tr>\n            <tr>\n              <th>Type</th>\n              <td>{{selectedCell?.cell.type}}</td>\n            </tr>\n            <tr>\n              <th>Team</th>\n              <td>{{selectedCell?.cell.team}}</td>\n            </tr>\n            <tr>\n              <th>Points</th>\n              <td>{{selectedCell?.cell.points}}</td>\n            </tr>\n          </table>\n\n        </ng-template>\n      </kendo-panelbar-item>\n      <kendo-panelbar-item [title]=\"'Actions'\" expanded=\"true\">\n        <ng-template kendoPanelBarContent>\n          <div class=\"panel-content\" style=\"display: flex; flex-direction: column;justify-content:center; align-content:center; align-items:center\">\n          <kendo-slider\n            [fixedTickWidth]=\"10\"\n            [min]=\"1\"\n            [max]=\"20\"\n            [smallStep]=\"true\"\n            [(ngModel)] = \"sliderPoint\"\n          ></kendo-slider>\n          <button kendoButton (click)=\"addPoints()\">Add {{sliderPoint}} points</button>\n          <button kendoButton (click)=\"subPoints()\">Substract {{sliderPoint}} points</button>\n          </div>\n        </ng-template>\n      </kendo-panelbar-item>\n\n      <kendo-panelbar-item [title]=\"'Turn history'\" expanded=\"true\">\n      </kendo-panelbar-item>\n    </kendo-panelbar>\n\n\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/components/board/board.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/board/board.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#chart {\n  background-image: url(\"/assets/grass.jpg\");\n  background-repeat: repeat-x repeat-y;\n  background-size: 240px 240px; }\n\n#controller {\n  width: 300px; }\n\n.panel-content {\n  margin-bottom: 12px;\n  margin-top: 12px; }\n"

/***/ }),

/***/ "./src/app/components/board/board.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/board/board.component.ts ***!
  \*****************************************************/
/*! exports provided: BoardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardComponent", function() { return BoardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! d3 */ "./node_modules/d3/index.js");
/* harmony import */ var d3_hexbin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! d3-hexbin */ "./node_modules/d3-hexbin/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CellType;
(function (CellType) {
    CellType["WALL"] = "WALL";
    CellType["FIELD"] = "FIELD";
    CellType["HOME"] = "HOME";
    CellType["TREASURE"] = "TREASURE";
    CellType["MINE"] = "MINE";
    CellType["DUNGEON"] = "DUNGEON";
    CellType["CASTLE"] = "CASTLE";
})(CellType || (CellType = {}));
var Team;
(function (Team) {
    Team["RED"] = "red";
    Team["BLUE"] = "blue";
    Team["BLACK"] = "black";
    Team["GREEN"] = "green";
    Team["WHITE"] = "white";
})(Team || (Team = {}));
var Cell = /** @class */ (function () {
    function Cell() {
    }
    return Cell;
}());
var Game = /** @class */ (function () {
    function Game() {
        this.turnPointer = 0;
        this.turnOrder = [Team.WHITE, Team.GREEN, Team.BLACK, Team.BLUE, Team.RED];
    }
    Game.prototype.getTurnTeam = function () {
        return this.turnOrder[this.turnPointer];
    };
    Game.prototype.nextTeamTurn = function () {
        this.turnPointer = (this.turnPointer + 1) % this.turnOrder.length;
    };
    return Game;
}());
var BoardComponent = /** @class */ (function () {
    function BoardComponent() {
        this.sliderPoint = 5;
        this.cellsX = 12;
        this.cellsY = 12;
        this.colors = {
            WALL: 'darkred'
        };
        this.model = {};
        this.game = new Game();
        this.width = window.innerWidth - 300;
        this.height = window.innerHeight;
        this.state = [];
        for (var i = 0; i < this.cellsY; i++) {
            this.state[i] = [];
            for (var j = 0; j < this.cellsX; j++) {
                var cell = new Cell();
                cell.type = CellType.FIELD;
                cell.points = 0;
                this.state[i][j] = cell;
            }
        }
        for (var i = 0; i < this.cellsX; i++) {
            this.state[0][i].type = CellType.WALL;
            this.state[i][0].type = CellType.WALL;
            this.state[i][11].type = CellType.WALL;
            this.state[9][i].type = CellType.WALL;
        }
        this.state[2][1].type = CellType.WALL;
        this.state[4][1].type = CellType.WALL;
        this.state[6][1].type = CellType.WALL;
        this.state[8][1].type = CellType.WALL;
        this.state[4][6].type = CellType.CASTLE;
        this.state[3][4].type = CellType.DUNGEON;
        this.state[3][6].type = CellType.DUNGEON;
        this.state[5][8].type = CellType.DUNGEON;
        this.state[6][6].type = CellType.DUNGEON;
        this.state[5][7].type = CellType.MINE;
        this.state[5][4].type = CellType.MINE;
        this.state[1][8].type = CellType.TREASURE;
        this.state[2][4].type = CellType.TREASURE;
        this.state[3][8].type = CellType.TREASURE;
        this.state[3][8].type = CellType.TREASURE;
        this.state[4][3].type = CellType.TREASURE;
        this.state[4][10].type = CellType.TREASURE;
        this.state[5][1].type = CellType.TREASURE;
        this.state[5][5].type = CellType.TREASURE;
        this.state[6][9].type = CellType.TREASURE;
        this.state[7][3].type = CellType.TREASURE;
        this.state[7][7].type = CellType.TREASURE;
        this.state[1][10].type = CellType.HOME;
        this.state[1][10].team = Team.RED;
        this.state[1][10].points = 20;
        this.state[8][10].type = CellType.HOME;
        this.state[8][10].team = Team.BLUE;
        this.state[8][10].points = 20;
        this.state[1][6].type = CellType.HOME;
        this.state[1][6].team = Team.BLACK;
        this.state[1][6].points = 20;
        this.state[8][2].type = CellType.HOME;
        this.state[8][2].team = Team.GREEN;
        this.state[8][2].points = 20;
        this.state[8][6].type = CellType.HOME;
        this.state[8][6].team = Team.WHITE;
        this.state[8][6].points = 20;
    }
    BoardComponent.prototype.ngOnInit = function () {
    };
    BoardComponent.prototype.ngAfterViewInit = function () {
        this.drawSvg();
    };
    BoardComponent.prototype.finishTurn = function () {
        this.game.nextTeamTurn();
    };
    BoardComponent.prototype.addPoints = function () {
        this.selectedCell.cell.points = this.selectedCell.cell.points + this.sliderPoint;
        this.selectedCell.cell.team = this.game.getTurnTeam();
        this.drawSvg();
    };
    BoardComponent.prototype.subPoints = function () {
        this.selectedCell.cell.points = this.selectedCell.cell.points - this.sliderPoint;
        this.selectedCell.cell.team = this.selectedCell.cell.points === 0 ? null : this.game.getTurnTeam();
        this.drawSvg();
    };
    BoardComponent.prototype.drawSvg = function () {
        var _this = this;
        d3__WEBPACK_IMPORTED_MODULE_1__["select"]('svg').remove();
        var hexRadius = Math.floor(this.height / 20);
        var points = [];
        for (var i = 0; i < this.cellsY - 2; i++) {
            for (var j = 0; j < this.cellsX; j++) {
                points.push([hexRadius * j * 1.75, hexRadius * i * 1.5]);
            }
        }
        points[0] = [1000, 1000];
        points[2 * this.cellsX] = [2000, 2000];
        points[4 * this.cellsX] = [3000, 3000];
        points[6 * this.cellsX] = [4000, 4000];
        points[8 * this.cellsX] = [5000, 5000];
        points[9 * this.cellsX] = [6000, 6000];
        points[9 * this.cellsX + this.cellsY - 1] = [7000, 7000];
        this.svg = d3__WEBPACK_IMPORTED_MODULE_1__["select"]('#chart').append('svg')
            .attr('width', this.width)
            .attr('height', this.height)
            .append('g')
            .attr('transform', 'translate(' + 250 + ',' + 100 + ')');
        var hexbin = d3_hexbin__WEBPACK_IMPORTED_MODULE_2__["hexbin"]().radius(hexRadius);
        this.svg.append('g')
            .selectAll('.hexagon')
            .data(hexbin(points))
            .enter().append('path')
            .attr('class', 'hexagon')
            .attr('d', function (d) {
            return 'M' + d.x + ',' + d.y + hexbin.hexagon();
        })
            .attr('stroke', 'white')
            .attr('stroke-width', function (pos, i) {
            var cell = _this.state[Math.floor(i / _this.cellsY)][i % _this.cellsX];
            return cell && _this.selectedCell && cell === _this.selectedCell.cell ? '5px' : '1px';
        })
            .style('fill', function (d, i) {
            var cell = _this.state[Math.floor(i / _this.cellsY)][i % _this.cellsX];
            if (cell.type === CellType.HOME) {
                return cell.team;
            }
            if (cell.type === CellType.FIELD && cell.team) {
                return cell.team;
            }
            return _this.colors[cell.type] || 'teal';
        });
        for (var row = 1; row <= 10; row++) {
            for (var col = 1; col <= 10; col++) {
                var point = points[Math.floor(row) * this.cellsY + col];
                var cell = this.state[row][col];
                switch (cell.type) {
                    case CellType.DUNGEON:
                        this.svg.append('g')
                            .selectAll('.hexagon')
                            .data(hexbin([point]))
                            .enter()
                            .append('image')
                            .attr('class', 'cell-image')
                            .attr('xlink:href', '/assets/dungeon.png')
                            .attr('width', 70)
                            .attr('height', 70)
                            .attr('x', function (d) { return d.x - 35; })
                            .attr('y', function (d) { return d.y - 40; });
                        break;
                    case CellType.CASTLE:
                        this.svg.append('g')
                            .selectAll('.hexagon')
                            .data(hexbin([point]))
                            .enter()
                            .append('image')
                            .attr('class', 'cell-image')
                            .attr('xlink:href', '/assets/castle.png')
                            .attr('width', 65)
                            .attr('height', 65)
                            .attr('x', function (d) { return d.x - 31; })
                            .attr('y', function (d) { return d.y - 35; });
                        break;
                    case CellType.MINE:
                        this.svg.append('g')
                            .selectAll('.hexagon')
                            .data(hexbin([point]))
                            .enter()
                            .append('image')
                            .attr('class', 'cell-image')
                            .attr('xlink:href', '/assets/pickaxe.png')
                            .attr('width', 65)
                            .attr('height', 65)
                            .attr('x', function (d) { return d.x - 31; })
                            .attr('y', function (d) { return d.y - 35; });
                        break;
                    case CellType.HOME:
                        this.svg.append('g')
                            .selectAll('.hexagon')
                            .data(hexbin([point]))
                            .enter()
                            .append('text')
                            .attr('class', 'cell-image')
                            .attr('x', function (d) { return d.x; })
                            .attr('y', function (d) { return d.y; })
                            .attr('alignment-baseline', 'middle')
                            .attr('text-anchor', 'middle')
                            .attr('font-weight', 'bold')
                            .attr('font-size', '24px')
                            .attr('fill', 'gray')
                            .text(cell.points);
                        break;
                    case CellType.FIELD:
                        if (cell.points > 0) {
                            this.svg.append('g')
                                .selectAll('.hexagon')
                                .data(hexbin([point]))
                                .enter()
                                .append('text')
                                .attr('class', 'cell-image')
                                .attr('x', function (d) { return d.x; })
                                .attr('y', function (d) { return d.y; })
                                .attr('alignment-baseline', 'middle')
                                .attr('text-anchor', 'middle')
                                .attr('font-weight', 'bold')
                                .attr('font-size', '24px')
                                .attr('fill', 'gray')
                                .text(cell.points);
                        }
                        break;
                    case CellType.TREASURE:
                        console.log("points", point);
                        this.svg.append('g')
                            .selectAll('.hexagon')
                            .data(hexbin([point]))
                            .enter()
                            .append('image')
                            .attr('class', 'cell-image')
                            .attr('xlink:href', '/assets/treasure.png')
                            .attr('width', 65)
                            .attr('height', 65)
                            .attr('x', function (d) { return d.x - 31; })
                            .attr('y', function (d) { return d.y - 35; });
                        break;
                }
            }
            d3__WEBPACK_IMPORTED_MODULE_1__["selectAll"]('.hexagon').on('mouseover', function (e) {
                var x = e[0][0];
                var y = e[0][1];
                var r = y / (hexRadius * 1.5);
                var c = x / (hexRadius * 1.75);
                _this.mouseOverRow = r;
                _this.mouseOverCol = c;
            });
            d3__WEBPACK_IMPORTED_MODULE_1__["selectAll"]('.hexagon, .cell-image').on('click', function (e) {
                if (_this.selectedCell && _this.selectedCell.row === _this.mouseOverRow && _this.selectedCell.col === _this.mouseOverCol) {
                    _this.selectedCell = null;
                }
                else {
                    _this.selectedCell = {
                        row: _this.mouseOverRow,
                        col: _this.mouseOverCol,
                        cell: _this.state[_this.mouseOverRow][_this.mouseOverCol]
                    };
                }
                _this.drawSvg();
            });
            // svg.append('g')
            //   .selectAll('.hexagon')
            //   .data(hexbin(points))
            //   .enter()
            //   .append('circle')
            //   .attr('cx', d => d.x)
            //   .attr('cy', d => d.y)
            //   .attr('r', 20)
            //     .attr('fill', 'lightblue');
            //
            // svg.append('g')
            //   .selectAll('.hexagon')
            //   .data(hexbin(points))
            //   .enter()
            //   .append('text')
            //   .attr('x', d => d.x)
            //   .attr('y', d => d.y)
            //   .attr('alignment-baseline', 'middle')
            //   .attr('text-anchor', 'middle')
            //   .attr('fill', 'darkblue')
            //   .text('10');
            //
            // svg.append('g')
            //   .selectAll('.hexagon')
            //   .data(hexbin(points))
            //   .enter()
            //   .append('image')
            //   .attr('xlink:href', '/assets/dungeon.png')
            //   .attr('width', 60)
            //   .attr('height', 60)
            //   .attr('x', d => d.x)
            //   .attr('y', d => d.y)
        }
    };
    BoardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'mom-board',
            template: __webpack_require__(/*! ./board.component.html */ "./src/app/components/board/board.component.html"),
            styles: [__webpack_require__(/*! ./board.component.scss */ "./src/app/components/board/board.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BoardComponent);
    return BoardComponent;
}());

// svg.append('rect')
//   .attr('width', hexRadius * 22)
//   .attr('height', hexRadius * 19)
//   .attr('fill', '')
//   .attr('transform', 'translate(-' + hexRadius + ',-' + hexRadius + ')');


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\dev\mom\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map